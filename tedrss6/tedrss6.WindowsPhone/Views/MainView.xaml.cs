﻿using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента пустой страницы см. по адресу http://go.microsoft.com/fwlink/?LinkID=390556

namespace tedrss6.Views
{
    /// <summary>
    ///     Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class MainView : Page
    {
        public MainView()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Вызывается перед отображением этой страницы во фрейме.
        /// </summary>
        /// <param name="e">
        ///     Данные события, описывающие, каким образом была достигнута эта страница.
        ///     Этот параметр обычно используется для настройки страницы.
        /// </param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
    }
}