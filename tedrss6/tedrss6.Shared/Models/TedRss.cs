﻿using System.Xml.Serialization;

namespace tedrss6.Models
{
    [XmlRoot("rss", IsNullable = false)]
    public class TedRss
    {
        [XmlElement("channel")]
        public TedVideoCollection Collection { get; set; }
    }
}