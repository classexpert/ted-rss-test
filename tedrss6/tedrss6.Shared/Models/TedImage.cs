﻿using System.Xml.Serialization;

namespace tedrss6.Models
{
    public class TedImage
    {
        [XmlAttribute("url")]
        public string Url { get; set; }
    }
}