﻿using System.Xml.Serialization;

namespace tedrss6.Models
{
    public class TedEnclosure
    {
        [XmlAttribute("url")]
        public string Url { get; set; }
    }
}