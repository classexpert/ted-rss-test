﻿using System.Xml.Serialization;

namespace tedrss6.Models
{
    public class TedVideo
    {
        [XmlElement("title")]
        public string Title { get; set; }

        [XmlElement("link")]
        public string Link { get; set; }

        [XmlElement("description")]
        public string Description { get; set; }

        [XmlElement(ElementName = "duration", Namespace = "http://www.itunes.com/dtds/podcast-1.0.dtd")]
        public string Duration { get; set; }

        [XmlElement("pubDate")]
        public string PubDate { get; set; }

        [XmlElement(ElementName = "image", Namespace = "http://www.itunes.com/dtds/podcast-1.0.dtd")]
        public TedImage Image { get; set; }

        [XmlElement(ElementName = "enclosure")]
        public TedEnclosure Enclosure { get; set; }

        [XmlElement(ElementName = "group", Namespace = "http://search.yahoo.com/mrss/")]
        public TedGroup ContentGroup { get; set; }
    }
}