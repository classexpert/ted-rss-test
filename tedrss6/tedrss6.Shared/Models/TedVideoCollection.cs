﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace tedrss6.Models
{
    public class TedVideoCollection
    {
        [XmlElement("link")]
        public string Link { get; set; }

        [XmlElement("title")]
        public string Title { get; set; }

        [XmlElement("description")]
        public string Description { get; set; }

        [XmlElement("item")]
        public List<TedVideo> Items { get; set; }
    }
}