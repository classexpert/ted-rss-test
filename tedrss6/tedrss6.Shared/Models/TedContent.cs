﻿using System.Xml.Serialization;
using tedrss6.Extensions;

namespace tedrss6.Models
{
    
    public class TedContent
    {
        [XmlAttribute("url")]
        public string Url { get; set; }

        [XmlAttribute("duration")]
        public string Duration { get; set; }

        [XmlAttribute("fileSize")]
        public long FileSize { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("bitrate")]
        public int Bitrate { get; set; }

        public string FileSizeString { get { return StringHelpers.BytesInString(FileSize); } }
    }
}