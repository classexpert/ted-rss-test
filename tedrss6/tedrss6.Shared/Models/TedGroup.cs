﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace tedrss6.Models
{
    public class TedGroup
    {
        [XmlElement(ElementName = "content", Namespace = "http://search.yahoo.com/mrss/")]
        public List<TedContent> Content { get; set; }
    }
}