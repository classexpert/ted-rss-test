﻿using System.Collections.Generic;
using System.Diagnostics;
using Windows.UI.Xaml.Controls;
using Caliburn.Micro;
using tedrss6.Models;
using tedrss6.Services;

namespace tedrss6.ViewModels
{
    public class MainViewModel : Screen
    {
        private readonly INavigationService _navigationService;

        private readonly ITedRssParser _tedParser;
        public MainViewModel(INavigationService navigationService, ITedRssParser tedParser)
        {
            _navigationService = navigationService;
            _tedParser = tedParser;
        }

        private List<TedVideo> _items;
        public List<TedVideo> Items
        {
            get { return _items; }
            set
            {
                _items = value;
                NotifyOfPropertyChange(() => Items);
            }
        }

        public void ItemClick(object sender, ItemClickEventArgs e)
        {
            var item=e.ClickedItem as TedVideo;
            if (item != null)
            {
                _navigationService.NavigateToViewModel<VideoViewModel>(item);
            }
        }

        protected async override void OnInitialize()
        {
            Debug.WriteLine("OnActivate");
            base.OnInitialize();
        }

        protected async override void OnActivate()
        {
            if (Items == null)
            {
                Debug.WriteLine("Load Items");
                Items = await _tedParser.GetVideoListDefault();
            }
            base.OnActivate();
        }

    }
}