﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;
using Caliburn.Micro;
using tedrss6.Models;

namespace tedrss6.ViewModels
{
    public class VideoViewModel : Screen
    {
        private bool _isInteractive = true;
        private TedVideo _parameter;

        private string _streamUrl;

        public TedVideo Parameter
        {
            get { return _parameter; }
            set
            {
                _parameter = value;
                NotifyOfPropertyChange(() => Parameter);
            }
        }

        public string StreamUrl
        {
            get { return _streamUrl; }
            set
            {
                _streamUrl = value;
                NotifyOfPropertyChange(() => StreamUrl);
            }
        }

        public bool IsInteractive
        {
            get { return _isInteractive; }
            set
            {
                _isInteractive = value;
                NotifyOfPropertyChange(() => IsInteractive);
            }
        }

        public void Tapped(object sender, TappedRoutedEventArgs e)
        {
            var content = (e.OriginalSource as FrameworkElement).DataContext as TedContent;
            if (content != null)
                UpdateStreamUrl(content.Url);
        }

        private void UpdateStreamUrl(string url)
        {
            if (StreamUrl != url)
            {
                StreamUrl = null;
                StreamUrl = url;
            }
        }

        protected override void OnActivate()
        {
            StreamUrl = Parameter.Enclosure.Url;
            base.OnActivate();
        }
    }
}