﻿using System.Globalization;

namespace tedrss6.Extensions
{
    public static class StringHelpers
    {
        public static string BytesInString(long size)
        {
            if (size <= 1048576)
                return (size/1024f).ToString("0.#", CultureInfo.InvariantCulture) + " Kb";
            if (size <= 1073741824)
                return (size/1048576f).ToString("0.#", CultureInfo.InvariantCulture) + " Mb";

            return (size/1073741824f).ToString("0.#", CultureInfo.InvariantCulture) + " Gb";
        }
    }
}