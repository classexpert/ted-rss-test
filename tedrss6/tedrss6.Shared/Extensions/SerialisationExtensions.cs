﻿using System.IO;
using System.Xml.Serialization;

namespace tedrss6.Extensions
{
    public static class SerialisationExtensions
    {
        public static string Serialize<T>(this T obj)
        {
            using (var sw = new StringWriter())
            {
                var serializer = new XmlSerializer(obj.GetType());
                serializer.Serialize(sw, obj);
                return sw.ToString();
            }
        }

        public static T Deserialize<T>(this string serialized)
        {
            using (var sw = new StringReader(serialized))
            {
                var serializer = new XmlSerializer(typeof (T));
                return (T) serializer.Deserialize(sw);
            }
        }
    }
}