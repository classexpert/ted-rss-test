﻿// Шаблон пустого приложения задокументирован по адресу http://go.microsoft.com/fwlink/?LinkId=234227

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml.Controls;
using Caliburn.Micro;
using tedrss6.Services;
using tedrss6.ViewModels;
using tedrss6.Views;

namespace tedrss6
{
    /// <summary>
    ///     Обеспечивает зависящее от конкретного приложения поведение, дополняющее класс Application по умолчанию.
    /// </summary>
    public sealed partial class App
    {
        private WinRTContainer container;

        /// <summary>
        ///     Инициализирует одноэлементный объект приложения. Это первая выполняемая строка разрабатываемого
        ///     кода; поэтому она является логическим эквивалентом main() или WinMain().
        /// </summary>
        public App()
        {
            InitializeComponent();
        }

        protected override void Configure()
        {
            container = new WinRTContainer();

            container.RegisterWinRTServices();

            container.PerRequest<MainViewModel>();
            container.PerRequest<VideoViewModel>();
            container.PerRequest<ITedRssParser, TedRssParser>();
        }

        protected override void PrepareViewFirst(Frame rootFrame)
        {
            container.RegisterNavigationService(rootFrame);
        }

        /// <summary>
        ///     Вызывается при обычном запуске приложения пользователем.  Будут использоваться другие точки входа,
        ///     если приложение запускается для открытия конкретного файла, отображения
        ///     результатов поиска и т. д.
        /// </summary>
        /// <param name="e">Сведения о запросе и обработке запуска.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs e)
        {
            DisplayRootView<MainView>();
        }

        protected override object GetInstance(Type service, string key)
        {
            return container.GetInstance(service, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            container.BuildUp(instance);
        }
    }
}