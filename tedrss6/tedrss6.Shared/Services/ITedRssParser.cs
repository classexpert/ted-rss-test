﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using tedrss6.Models;

namespace tedrss6.Services
{
    public interface ITedRssParser
    {
        Task<List<TedVideo>> GetVideoListDefault();
        Task<List<TedVideo>> GetVideoList(Uri url);
    }
}
