﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.Web.Http;
using tedrss6.Extensions;
using tedrss6.Models;

namespace tedrss6.Services
{
    public class TedRssParser : ITedRssParser
    {
        public Task<List<TedVideo>> GetVideoListDefault()
        {
            return GetVideoList(new Uri("http://www.ted.com/themes/rss/id/6"));
        }

        public async Task<List<TedVideo>> GetVideoList(Uri url)
        {
            using (var client = new HttpClient())
            {
                using (HttpResponseMessage response = await client.GetAsync(url))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        string result = await response.Content.ReadAsStringAsync();
                        var rssFeed = result.Deserialize<TedRss>();
                        return rssFeed.Collection.Items;
                    }
                }
            }
            return null;
        }
    }
}